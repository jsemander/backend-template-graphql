import 'mocha';
import * as mongoose from 'mongoose';
import { AppRunner, BatchRunner } from '../../src/app/runner';
import { ConfigSchema } from '../../src/models/config';
import TestDbHelper from '../helpers/db';

const db = new TestDbHelper();

describe('AppRunner', () => {
    before(() => db.start());
    after(() => db.stop());
    it('should run', () => {
        return db.getConnectionString().then((uri) => {
            return new Promise((resolve, reject) => {
                mongoose.connect(uri, {
                    useNewUrlParser: true,
                }, (err) => {
                    if (err) {
                        return reject(err);
                    }
                    const configModel = mongoose.model('Config', ConfigSchema);
                    const config = new configModel({
                        name: 'Version',
                        value: '2.0.0',
                    });
                    return config.save().then(() => {
                        return AppRunner(uri, {
                            Cron: {},
                            JWT: {
                                secret: Buffer.from(Math.random().toString(36)).toString('base64'),
                            },
                            Name: 'Backend Template',
                            Port: 3000,
                            Redis: {
                                enabled: false,
                            },
                            Timezone: 'UTC',
                            Type: 'API',
                            Version: '1.0.0',
                        }).then((app) => {
                            // Kill Socket Server
                            app.socket.io.close(() => {
                                // Kill HTTP Server
                                app.server.close(() => {
                                    // Finish Test
                                    return resolve();
                                });
                            });
                        });
                    });
                });
            });
        }).then(() => mongoose.disconnect());
    });
});
describe('BatchRunner', () => {
    before(() => db.start());
    after(() => db.stop());
    it('should run with default timeout', () => {
        return db.getConnectionString().then((uri) => {
            return BatchRunner(uri, {
                Cron: {},
                JWT: {
                    secret: Buffer.from(Math.random().toString(36)).toString('base64'),
                },
                Name: 'Backend Template',
                Port: 3001,
                Redis: {
                    enabled: false,
                },
                Timezone: 'UTC',
                Type: 'API',
                Version: '1.0.0',
            }).then(() => mongoose.disconnect());
        });
    });
    it('should run with set timeout', () => {
        return db.getConnectionString().then((uri) => {
            return BatchRunner(uri, {
                Cron: {
                    timeout: 1000,
                },
                JWT: {
                    secret: Buffer.from(Math.random().toString(36)).toString('base64'),
                },
                Name: 'Backend Template',
                Port: 3001,
                Redis: {
                    enabled: false,
                },
                Timezone: 'UTC',
                Type: 'API',
                Version: '1.0.0',
            }).then(() => mongoose.disconnect());
        });
    });
});
