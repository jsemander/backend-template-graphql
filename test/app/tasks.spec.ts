import { assert, expect } from 'chai';
import 'mocha';
import * as mongoose from 'mongoose';
import { IAppConfig } from '../../src/app';
import { ITasks, Tasks } from '../../src/app/tasks';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let models: IModels;
let resolvers: IResolvers;
let tasks: ITasks;

describe('Tasks', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    resolvers = Resolvers(configs, models);
                    tasks = Tasks(configs, resolvers);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    describe('Session', () => {
        describe('#clean', () => {
            it('should clean expired sessions', () => {
                return tasks.session.clean().then((result) => {
                    expect(result).to.have.property('success', true);
                });
            });
        });
    });
});
