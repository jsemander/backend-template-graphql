import 'mocha';

describe('Socket', () => {
    before(() => {});
    after(() => {});
    it('should disconnect without token');
    it('should connect with token without existing sessions');
    it('should connect with token with existing sessions');
});
