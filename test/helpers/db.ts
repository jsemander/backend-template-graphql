import { MongoMemoryServer } from 'mongodb-memory-server';

export default class TestDbHelper {
    /**
     * MongoDB Memory Server Instance
     */
    protected mongod: MongoMemoryServer;
    constructor() {
        this.mongod = new MongoMemoryServer({
            autoStart: false,
            debug: process.env.MONGO_DEBUG === 'yes' || false,
        });
    }
    /**
     * Get Database URL
     */
    public getConnectionString(): Promise<string> {
        if (process.env.MONGO_MEMORY === 'yes') {
            return this.mongod.getConnectionString();
        } else if (process.env.DB_HOST) {
            return Promise.resolve(process.env.DB_HOST);
        }
        return Promise.reject({
            message: 'The database is not connected',
        });
    }
    /**
     * Start the instance
     */
    public start(): Promise<boolean> {
        if (process.env.MONGO_MEMORY === 'yes') {
            return this.mongod.start();
        }
        return Promise.resolve(true);
    }
    /**
     * Stop the instance
     */
    public stop(): Promise<boolean> {
        if (process.env.MONGO_MEMORY === 'yes') {
            return this.mongod.stop();
        }
        return Promise.resolve(true);
    }
}
