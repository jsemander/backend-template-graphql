import { assert, expect } from 'chai';
import 'mocha';
import { IJWTConfig, JWT } from '../../src/lib/JWT';

const config: IJWTConfig = {
    secret: Buffer.from(Math.random().toString(36)).toString('base64'),
};
const data = {};
let jwt: JWT;
let token = '';

describe('JWT', () => {
    describe('Normal Tests', () => {
        beforeEach((done) => {
            config.secret = Buffer.from(Math.random().toString(36)).toString('base64');
            jwt = new JWT(config);
            return done();
        });
        describe('#createToken()', () => {
            it('should create a jwt token', () => {
                expect(jwt.createToken(data)).to.be.a('string');
            });
        });
        describe('#isAuthorized()', () => {
            it('should check an authorized token', (done) => {
                token = jwt.createToken(data);
                jwt.isAuthorized(token).then((subject) => {
                    expect(subject).to.be.deep.equal(data);
                    return done();
                }).catch((err) => {
                    assert.ok(false, err);
                    return done();
                });
            });
            it('should be an unauthorized token because the secret is different', (done) => {
                jwt.isAuthorized(token).then((subject) => {
                    assert.ok(false, subject);
                    return done();
                }).catch((err) => {
                    expect(err).to.be.an('error');
                    return done();
                });
            });
        });
    });
    // describe('Expired Tests', () => {
    //     beforeEach((done) => {
    //         config.secret = Buffer.from(Math.random().toString(36)).toString('base64');
    //         jwt = new JWT(config);
    //         return done();
    //     });
    //     it('should be an unauthorized token because the token is expired', (done) => {
    //         token = jwt.createToken(data);
    //         jwt.isAuthorized(token).then((subject) => {
    //             assert.ok(false, subject);
    //             return done();
    //         }).catch((err) => {
    //             expect(err).to.be.an('error');
    //             return done();
    //         });
    //     });
    // });
});
