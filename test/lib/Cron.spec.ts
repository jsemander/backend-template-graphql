import { assert, expect } from 'chai';
import * as _ from 'lodash';
import 'mocha';
import * as moment from 'moment-timezone';
import * as mongoose from 'mongoose';
import { Cron } from '../../src/lib/Cron';
import { IModels, Models } from '../../src/models';
import { Job } from '../../src/resolvers/job';
import TestDbHelper from '../helpers/db';

const db = new TestDbHelper();
let models: IModels;

describe('Cron without options', () => {
    after(() => mongoose.disconnect().then(() => db.stop()));
    it('should set timeout to default value', () => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    const cron = new Cron({}, new Job(m));
                });
            });
        });
    });
});
describe('Cron with options', () => {
    let job: Job;
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    job = new Job(m);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    afterEach(() => models.job.deleteMany({}));
    describe('#define()', () => {
        it('should deny blank model name', () => {
            const cron = new Cron({
                timeout: 1000,
            }, job);
            cron.define('model', 'success', () => {
                return Promise.resolve({
                    success: true,
                });
            });
            cron.define('model', 'fail', () => {
                return Promise.resolve({
                    message: 'Failed Action',
                    success: false,
                });
            });
            expect(cron.define('', '', () => Promise.resolve({
                error: 'Incorrect definition',
                success: false,
            })), 'Task was not defined').to.be.equal(false);
        });
        it('should deny blank action name', () => {
            const cron = new Cron({
                timeout: 1000,
            }, job);
            cron.define('model', 'success', () => {
                return Promise.resolve({
                    success: true,
                });
            });
            cron.define('model', 'fail', () => {
                return Promise.resolve({
                    message: 'Failed Action',
                    success: false,
                });
            });
            expect(cron.define('model', '', () => Promise.resolve({
                error: 'Incorrect definition',
                success: false,
            })), 'Task was not defined').to.be.equal(false);
        });
    });
    describe('#start()', () => {
        it('should resolve', () => {
            return new Promise((resolve, reject) => {
                const cron = new Cron({
                    timeout: 1000,
                }, job);
                cron.define('model', 'success', () => {
                    return Promise.resolve({
                        success: true,
                    });
                });
                cron.define('model', 'fail', () => {
                    return Promise.resolve({
                        message: 'Failed Action',
                        success: false,
                    });
                });
                cron.start().then((msg) => {
                    expect(msg).to.be.equal('The scheduler has started');
                    cron.stop().then(() => {
                        return resolve();
                    });
                }).catch(reject);
            });
        });
        it('should reject', () => {
            return new Promise((resolve) => {
                const cron = new Cron({
                    timeout: 1000,
                }, job);
                cron.define('model', 'success', () => {
                    return Promise.resolve({
                        success: true,
                    });
                });
                cron.define('model', 'fail', () => {
                    return Promise.resolve({
                        message: 'Failed Action',
                        success: false,
                    });
                });
                cron.start().then(() => {
                    cron.start().then((msg) => {
                        assert.fail('Cron should not have started');
                        cron.stop().then(() => resolve());
                    }).catch((msg) => {
                        expect(msg).to.be.equal('The scheduler is already running');
                        cron.stop().then(() => resolve());
                    });
                });
            });
        });
    });
    describe('#stop()', () => {
        it('should resolve', () => {
            return new Promise((resolve, reject) => {
                const cron = new Cron({
                    timeout: 1000,
                }, job);
                cron.define('model', 'success', () => {
                    return Promise.resolve({
                        success: true,
                    });
                });
                cron.define('model', 'fail', () => {
                    return Promise.resolve({
                        message: 'Failed Action',
                        success: false,
                    });
                });
                cron.start().then((msg) => {
                    expect(msg).to.be.equal('The scheduler has started');
                    cron.stop().then((msg2) => {
                        expect(msg2).to.be.equal('The scheduler has shut down');
                        return resolve();
                    }).catch(reject);
                });
            });
        });
        it('should reject', () => {
            return new Promise((resolve) => {
                const cron = new Cron({
                    timeout: 1000,
                }, job);
                cron.define('model', 'success', () => {
                    return Promise.resolve({
                        success: true,
                    });
                });
                cron.define('model', 'fail', () => {
                    return Promise.resolve({
                        message: 'Failed Action',
                        success: false,
                    });
                });
                cron.stop().then((msg) => {
                    assert.fail('Cron should not have stopped');
                    return resolve();
                }).catch((msg) => {
                    expect(msg).to.be.equal('The scheduler is not currently running');
                    return resolve();
                });
            });
        });
    });
    describe('#nextTask()', () => {
        it('should run task successfully', () => {
            const cron = new Cron({
                timeout: 1000,
            }, job);
            cron.define('model', 'success', () => {
                return Promise.resolve({
                    success: true,
                });
            });
            cron.define('model', 'fail', () => {
                return Promise.resolve({
                    message: 'Failed Action',
                    success: false,
                });
            });
            const record = new models.job({
                data: {},
                enabled: true,
                lastFinishedAt: new Date(),
                lastModifiedBy: '',
                lastRunAt: new Date(),
                name: 'Model.Success',
                nextRunAt: new Date(moment().subtract({
                    day: 1,
                }).toISOString()),
                repeatInterval: '0 2 * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'success',
                    model: 'model',
                },
            });
            return record.save().then(() => {
                return cron.start().then(() => {
                    return new Promise((resolve, reject) => {
                        cron.on('fail', (err) => {
                            cron.stop().then(() => assert.fail('Should not have failed')).catch(reject);
                        });
                        cron.on('error', (err) => {
                            cron.stop().then(() => assert.fail('Should not have errored')).catch(reject);
                        });
                        cron.on('success', () => {
                            cron.stop().then(() => {
                                assert.ok(true);
                                return resolve();
                            }).catch(reject);
                        });
                    });
                });
            });
        });
        it('should run task unsuccessfully by no method', () => {
            const cron = new Cron({
                timeout: 1000,
            }, job);
            cron.define('model', 'success', () => {
                return Promise.resolve({
                    success: true,
                });
            });
            cron.define('model', 'fail', () => {
                return Promise.resolve({
                    message: 'Failed Action',
                    success: false,
                });
            });
            const record = new models.job({
                data: {},
                enabled: true,
                lastFinishedAt: new Date(),
                lastModifiedBy: '',
                lastRunAt: new Date(),
                name: 'Model.Undefined',
                nextRunAt: new Date(moment().subtract({
                    day: 1,
                }).toISOString()),
                repeatInterval: '0 2 * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'undefined',
                    model: 'model',
                },
            });
            return record.save().then(() => {
                return cron.start().then(() => {
                    return new Promise((resolve, reject) => {
                        cron.on('fail', () => {
                            cron.stop().then(() => {
                                assert.fail('Should have errored');
                                return reject();
                            }).catch(reject);
                        });
                        cron.on('error', (err) => {
                            expect(err).to.have.property('message', 'Model.Undefined has been disabled');
                            cron.stop().then(() => resolve()).catch(reject);
                        });
                        cron.on('success', () => {
                            cron.stop().then(() => {
                                assert.fail('Should have errored');
                                return reject();
                            }).catch(reject);
                        });
                    });
                });
            });
        });
        it('should run task unsuccessfully by failure', () => {
            const cron = new Cron({
                timeout: 1000,
            }, job);
            cron.define('model', 'success', () => {
                return Promise.resolve({
                    success: true,
                });
            });
            cron.define('model', 'fail', () => {
                return Promise.resolve({
                    message: 'Failed Action',
                    success: false,
                });
            });
            const record = new models.job({
                data: {},
                enabled: true,
                lastFinishedAt: new Date(),
                lastModifiedBy: '',
                lastRunAt: new Date(),
                name: 'Model.Fail',
                nextRunAt: new Date(moment().subtract({
                    day: 1,
                }).toISOString()),
                repeatInterval: '0 2 * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'fail',
                    model: 'model',
                },
            });
            return record.save().then(() => {
                return cron.start().then(() => {
                    return new Promise((resolve, reject) => {
                        cron.on('fail', (err) => {
                            cron.stop().then(() => {
                                return resolve();
                            }).catch(reject);
                        });
                        cron.on('error', (err) => {
                            cron.stop().then(() => {
                                return resolve();
                            }).catch(reject);
                        });
                        cron.on('success', () => {
                            cron.stop().then(() => {
                                assert.fail('Should have failed');
                                return reject();
                            }).catch(reject);
                        });
                    });
                });
            });
        });
    });
});
