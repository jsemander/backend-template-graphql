import { assert, expect } from 'chai';
import 'mocha';
import * as mongoose from 'mongoose';
import { IAppConfig } from '../../src/app';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let models: IModels;
let resolvers: IResolvers;

describe('Resolvers:Role', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    resolvers = Resolvers(configs, models);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    afterEach(() => models.role.deleteMany({}));
    describe('#create()', () => {
        it('should not create a role', () => {
            const role = {
                enabled: true,
                name: 'Admin',
                permissions: [],
            };
            return resolvers.role.create(role).then(() => {
                return resolvers.role.create(role).then(() => {
                    assert.fail('Role should not have been created');
                }).catch((err) => {
                    expect(err).to.have.property('message', `A role already exists with the same name: ${role.name}`);
                    expect(err).to.have.property('statusCode', 409);
                });
            });
        });
        it('should get a role', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.role.get(role._id);
            });
        });
    });
    describe('#get()', () => {
        it('should not get a role', () => {
            return resolvers.role.get(mongoose.Types.ObjectId().toHexString()).then(() => {
                assert.fail('Role should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No role found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#getName()', () => {
        it('should get a role by name', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.role.getByName(role.name);
            });
        });
        it('should not get a role by name', () => {
            return resolvers.role.getByName('Agent').then(() => {
                assert.fail('Role should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No role found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#list()', () => {
        it('should get a list of roles', () => {
            return resolvers.role.list({}, {
                limit: 25,
                page: 1,
                sort: {
                    name: 1,
                },
            });
        });
    });
    describe('#update()', () => {
        it('should update a role', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.role.update(role._id, {
                    enabled: true,
                    name: 'Agent',
                    permissions: [],
                });
            });
        });
        it('should not update a role because name', () => {
            return Promise.all([
                resolvers.role.create({
                    name: 'Admin',
                    permissions: [],
                }),
                resolvers.role.create({
                    name: 'Agent',
                    permissions: [],
                }),
            ]).then((roles) => {
                return resolvers.role.update(roles[0]._id, {
                    enabled: true,
                    name: 'Agent',
                    permissions: [],
                }).then(() => {
                    assert.fail('Role should not have been found');
                }).catch((err) => {
                    expect(err).to.have.property(
                        'message',
                        `A role already exists with the same name: ${roles[1].name}`,
                    );
                    expect(err).to.have.property('statusCode', 409);
                });
            });
        });
        it('should not update a role because missing', () => {
            return resolvers.role.update(mongoose.Types.ObjectId().toHexString(), {
                enabled: true,
                name: 'Agent',
                permissions: [],
            }).then(() => {
                assert.fail('Role should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No role found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
});
