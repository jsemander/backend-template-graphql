import { assert, expect } from 'chai';
import * as parser from 'cron-parser';
import 'mocha';
import * as moment from 'moment-timezone';
import * as mongoose from 'mongoose';
import { IAppConfig } from '../../src/app';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let models: IModels;
let resolvers: IResolvers;

describe('Resolvers:Job', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    resolvers = Resolvers(configs, models);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    afterEach(() => models.job.deleteMany({}));
    describe('#create()', () => {
        it('should not create a job because it is a duplicate', () => {
            return resolvers.job.create({
                data: {
                    _id: mongoose.Types.ObjectId().toHexString(),
                },
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.create({
                    data: record.data,
                    name: record.name,
                    nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                    repeatInterval: record.repeatInterval,
                    repeatTimezone: 'America/New_York',
                    task: record.task,
                }).then(() => {
                    assert.fail('Job should not have been created');
                }).catch((err) => {
                    expect(err).to.have.property('message', 'A job already exists by the specified configuration');
                    expect(err).to.have.property('statusCode', 409);
                });
            });
        });
        it('should create a job', () => {
            return resolvers.job.create({
                data: {
                    _id: mongoose.Types.ObjectId().toHexString(),
                },
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.create({
                    data: {
                        _id: mongoose.Types.ObjectId().toHexString(),
                    },
                    name: record.name,
                    nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                    repeatInterval: record.repeatInterval,
                    repeatTimezone: 'America/New_York',
                    task: record.task,
                });
            });
        });
    });
    describe('#disable()', () => {
        it('should disable the record', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.disable(record._id).then((r2) => {
                    expect(r2).to.have.property('enabled');
                    expect(r2.enabled).to.be.equal(false);
                });
            });
        });
        it('should not find any record', () => {
            return resolvers.job.disable(mongoose.Types.ObjectId().toHexString()).then(() => {
                assert.fail('Job update should not have passed');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No job found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#finish()', () => {
        it('should finish the record', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.finish(record._id).then((r1) => {
                    if (r1) {
                        return resolvers.job.finish(record._id).then((r2) => {
                            if (r2) {
                                expect(r2).to.have.property('lastFinishedAt');
                                expect(r2.lastFinishedAt).to.be.greaterThan(r1.lastFinishedAt);
                            }
                        });
                    }
                });
            });
        });
        it('should delete record after finished', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.running(record._id).then(() => {
                    return resolvers.job.finish(record._id).then(() => {
                        return resolvers.job.get(record._id).then((r) => {
                            expect(r).to.be.a('null');
                        });
                    });
                });
            });
        });
        it('should not find any record', () => {
            return resolvers.job.finish(mongoose.Types.ObjectId().toHexString()).then(() => {
                assert.fail('Job should not have finished');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No job found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
    describe('#get()', () => {
        it('should find the record', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.get(record._id).then((r2) => {
                    expect(r2).to.have.property('name', 'Model.Action');
                });
            });
        });
        it('should not find any record', () => {
            return resolvers.job.get(mongoose.Types.ObjectId().toHexString()).then((r) => {
                expect(r).to.be.a('null');
            });
        });
    });
    describe('#getNext()', () => {
        it('should find the record', () => {
            return Promise.all([
                resolvers.job.create({
                    data: {},
                    name: 'Next Job',
                    nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                    repeatInterval: '*/1 * * * *',
                    repeatTimezone: 'America/New_York',
                    task: {
                        action: 'action',
                        model: 'model',
                    },
                }),
                resolvers.job.create({
                    data: {},
                    name: 'Not Next Job',
                    nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                    repeatInterval: '*/1 * * * *',
                    repeatTimezone: 'America/New_York',
                    task: {
                        action: 'action',
                        model: 'model',
                    },
                }),
            ]).then(() => {
                return resolvers.job.getNext().then((r) => {
                    expect(r).to.have.property('name', 'Next Job');
                });
            });
        });
        it('should not find any record', () => {
            return resolvers.job.getNext().then((r) => {
                expect(r).to.be.a('undefined');
            });
        });
    });
    describe('#list()', () => {
        it('should retireve a list of jobs', () => {
            return resolvers.job.list({}, {
                limit: 25,
                page: 1,
                sort: {
                    nextRunAt: 1,
                },
            });
        });
    });
    describe('#remove()', () => {
        it('should remove jobs', () => {
            return resolvers.job.remove({});
        });
    });
    describe('#running()', () => {
        it('should not find any record', () => {
            return resolvers.job.running(mongoose.Types.ObjectId().toHexString()).then(() => {
                assert.fail('Job should not have ran');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No job found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
        it('should set nextRunAt to incremented', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.running(record._id).then((r2) => {
                    const interval = parser.parseExpression(record.repeatInterval, {
                        tz: 'America/New_York',
                    });
                    expect(r2).to.have.property('nextRunAt');
                    expect(r2.nextRunAt.toISOString()).to.be.equal(interval.next().toISOString());
                });
            });
        });
        it('should set nextRunAt to incremented with default timezone', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: '',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.running(record._id).then((r2) => {
                    const interval = parser.parseExpression(record.repeatInterval);
                    expect(r2).to.have.property('nextRunAt');
                    expect(r2.nextRunAt.toISOString()).to.be.equal(interval.next().toISOString());
                });
            });
        });
        it('should set nextRunAt to null', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.running(record._id).then((r2) => {
                    expect(r2).to.have.property('nextRunAt');
                    expect(r2.nextRunAt).to.be.a('null');
                });
            });
        });
    });
    describe('#update()', () => {
        it('should not update a job because it is a duplicate', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.create({
                    data: {},
                    name: record.name,
                    nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                    repeatInterval: '* */1 * * *',
                    repeatTimezone: 'America/New_York',
                    task: record.task,
                }).then((record2) => {
                    return resolvers.job.update(record2._id, Object.assign({}, record.toObject(), {
                        repeatInterval: record.repeatInterval,
                    })).then(() => {
                        assert.fail('Job should not have been updated');
                    }).catch((err) => {
                        expect(err).to.have.property('message', 'A job already exists by the specified configuration');
                        expect(err).to.have.property('statusCode', 409);
                    });
                });
            });
        });
        it('should not update any record', () => {
            return resolvers.job.update(mongoose.Types.ObjectId().toHexString(), {}).then(() => {
                assert.fail('Job should not have been updated');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No job found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
        it('should update a job', () => {
            return resolvers.job.create({
                data: {},
                name: 'Model.Action',
                nextRunAt: moment().subtract({ day: 1 }).toISOString(),
                repeatInterval: '*/1 * * * *',
                repeatTimezone: 'America/New_York',
                task: {
                    action: 'action',
                    model: 'model',
                },
            }).then((record) => {
                return resolvers.job.update(record._id, Object.assign({}, record.toObject(), {
                    data: {
                        value: true,
                    },
                })).then((r) => {
                    expect(r).to.have.property('data');
                    expect(r.data).to.have.property('value', true);
                });
            });
        });
    });
});
