import { assert, expect } from 'chai';
import * as faker from 'faker';
import 'mocha';
import * as moment from 'moment-timezone';
import * as mongoose from 'mongoose';
import { IAppConfig } from '../../src/app';
import { IModels, Models } from '../../src/models';
import { IResolvers, Resolvers } from '../../src/resolvers';
import TestDbHelper from '../helpers/db';

const configs: IAppConfig = {
    Cron: {},
    JWT: {
        secret: Buffer.from(Math.random().toString(36)).toString('base64'),
    },
    Name: 'Backend Template',
    Port: 3000,
    Redis: {
        enabled: false,
    },
    Timezone: 'UTC',
    Type: 'API',
    Version: '1.0.0',
};
const db = new TestDbHelper();
let models: IModels;
let resolvers: IResolvers;

describe('Resolvers:Session', () => {
    before(() => {
        return db.start().then(() => {
            return db.getConnectionString().then((uri) => {
                return Models(uri).then((m) => {
                    models = m;
                    resolvers = Resolvers(configs, models);
                });
            });
        });
    });
    after(() => mongoose.disconnect().then(() => db.stop()));
    afterEach(() => Promise.all([
        models.role.deleteMany({}),
        models.session.deleteMany({}),
        models.user.deleteMany({}),
    ]));
    describe('#create()', () => {
        it('should not create a session because the user does not exist', () => {
            return resolvers.session.create(mongoose.Types.ObjectId().toHexString(), mongoose.Types.ObjectId().toHexString()).then().then(() => {
                assert.fail('The session should not have been created');
            }).catch((err) => {
                expect(err).to.have.property('message', 'There was no user record found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
        it('should not create a session because the user is disabled', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    return resolvers.user.update(user._id.toHexString(), {
                        enabled: false,
                    }).then(() => {
                        return resolvers.session.create(user._id.toHexString(), mongoose.Types.ObjectId().toHexString()).then(() => {
                            assert.fail('The session should not have been created');
                        }).catch((err) => {
                            expect(err).to.have.property('message', 'Your user has been disabled');
                            expect(err).to.have.property('statusCode', 404);
                        });
                    });
                });
            });
        });
        it('should not create a session because the user\'s permission was revoked', () => {
            return resolvers.user.create({
                email: faker.internet.email(),
                first_name: faker.name.firstName(),
                last_name: faker.name.lastName(),
                middle_name: '',
                role: null,
                username: faker.internet.userName(),
                verified: true,
            }).then((user) => {
                return resolvers.session.create(user._id.toHexString(), mongoose.Types.ObjectId().toHexString()).then(() => {
                    assert.fail('The session should not have been created');
                }).catch((err) => {
                    expect(err).to.have.property('message', 'Your permission has been revoked');
                    expect(err).to.have.property('statusCode', 404);
                });
            });
        });
    });
    describe('#get()', () => {
        it('should not get a session by id because the session does not exist', () => {
            return resolvers.session.get(mongoose.Types.ObjectId().toHexString()).then().then(() => {
                assert.fail('The session should not have been found');
            }).catch((err) => {
                expect(err).to.have.property('message', 'There was no session record found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
        it('should not get a session by id because the user does not exist', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then((session) => {
                        return models.user.deleteMany({}).then(() => {
                            return resolvers.session.get(session._id.toHexString()).then().then(() => {
                                assert.fail('The session should not have been found');
                            }).catch((err) => {
                                expect(err).to.have.property('message', 'There was no user record found');
                                expect(err).to.have.property('statusCode', 404);
                            });
                        });
                    });
                });
            });
        });
        it('should not get a session by id because the user is disabled', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then((session) => {
                        return resolvers.user.update(user._id.toHexString(), {
                            enabled: false,
                        }).then(() => {
                            return resolvers.session.get(session._id.toHexString()).then().then(() => {
                                assert.fail('The session should not have been found');
                            }).catch((err) => {
                                expect(err).to.have.property('message', 'Your user has been disabled');
                                expect(err).to.have.property('statusCode', 404);
                            });
                        });
                    });
                });
            });
        });
        it('should not get a session by id because the user\'s permission was revoked', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then((session) => {
                        return resolvers.user.update(user._id.toHexString(), {
                            role: null,
                        }).then(() => {
                            return resolvers.session.get(session._id.toHexString()).then().then(() => {
                                assert.fail('The session should not have been found');
                            }).catch((err) => {
                                expect(err).to.have.property('message', 'Your permission has been revoked');
                                expect(err).to.have.property('statusCode', 404);
                            });
                        });
                    });
                });
            });
        });
        it('should not get a session by id because the session is expired', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then((session) => {
                        return resolvers.session.update(session._id.toHexString(), {
                            expiration: moment().startOf('day').toISOString(),
                        }).then(() => {
                            return resolvers.session.get(session._id.toHexString()).then().then(() => {
                                assert.fail('The session should not have been found');
                            }).catch((err) => {
                                expect(err).to.have.property('message', 'The session is expired');
                                expect(err).to.have.property('statusCode', 409);
                            });
                        });
                    });
                });
            });
        });
        it('should get a session by id', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then((session) => {
                        return resolvers.session.get(session._id.toHexString());
                    });
                });
            });
        });
    });
    describe('#getByUserId()', () => {
        it('should get a session by user id', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then(() => {
                        return resolvers.session.getByUserId(user._id.toHexString());
                    });
                });
            });
        });
    });
    describe('#getBySocketId()', () => {
        it('should get a session by socket id', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then(() => {
                        return resolvers.session.getBySocketId(socketId);
                    });
                });
            });
        });
    });
    describe('#removeById()', () => {
        it('should remove a session', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then((session) => {
                        return resolvers.session.removeById(session._id.toHexString());
                    });
                });
            });
        });
    });
    describe('#update()', () => {
        it('should update a session', () => {
            return resolvers.role.create({
                name: 'Admin',
                permissions: [],
            }).then((role) => {
                return resolvers.user.create({
                    email: faker.internet.email(),
                    first_name: faker.name.firstName(),
                    last_name: faker.name.lastName(),
                    middle_name: '',
                    role: role._id.toHexString(),
                    username: faker.internet.userName(),
                    verified: true,
                }).then((user) => {
                    const socketId = mongoose.Types.ObjectId().toHexString();
                    return resolvers.session.create(user._id.toHexString(), socketId).then((session) => {
                        return resolvers.session.update(session._id.toHexString(), {
                            socket: mongoose.Types.ObjectId().toHexString(),
                        });
                    });
                });
            });
        });
        it('should not update a session', () => {
            return resolvers.session.update(mongoose.Types.ObjectId().toHexString(), {
                socket: mongoose.Types.ObjectId().toHexString(),
            }).then(() => {
                assert.fail('Session should not have been authorized');
            }).catch((err) => {
                expect(err).to.have.property('message', 'No session found');
                expect(err).to.have.property('statusCode', 404);
            });
        });
    });
});
