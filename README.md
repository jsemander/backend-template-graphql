Backend-Template
=================

About
-----
This is a template to build a backend in Node.JS. This backend is tailored for GHW use, but can be modified to fit any need.


Unit Testing
-------
In order to run the unit tests for the template, please run the following command:

```
TEST_PASSWORD=Welcome01 bash -c 'yarn test'
```
