import { NextFunction, Request, Response, Router } from 'express';
import { compact, filter, find, forEach, isEqual, map } from 'lodash';
import { IPaginationOptions } from '../lib/Types';
import { IRolePermission } from '../models/roles';
import { IResolvers } from '../resolvers';
import { IRoleListParams } from '../resolvers/role';

export default (resolvers: IResolvers) => {
    const router = Router();
    
    /**
     * @api {get} /roles List Paginated Roles
     * @apiGroup Role
     * @apiName List Paginated Roles
     * @apiSuccess {Object[]} roles Array of roles
     * @apiVersion 2.0.0
     */
    router.get('/', (req: Request, res: Response, next: NextFunction) => {
        const params: IRoleListParams = {};
        const options: IPaginationOptions = {
            limit: 25,
            page: 1,
            sort: {
                name: 1,
            },
        };
        
        if (req.query.page) {
            options.page = parseInt(req.query.page,  10);
        }
        if (req.query.limit) {
            options.limit = parseInt(req.query.limit,  10);
        }
        if (req.query.sort) {
            options.sort = JSON.parse(req.query.sort);
        }
        if (req.query.status) {
            params.enabled = (req.query.status === 'Enabled');
        }
        return resolvers.role.list(params, options).then((roles) => {
            return res.json(roles);
        }).catch(next);
    });
    
    /**
     * @api {get} /roles/:id Get role by id
     * @apiGroup Role
     * @apiName Get role by id
     * @apiSuccess {Object} role Role
     * @apiVersion 2.0.0
     */
    router.get('/:id', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.role.get(req.params.id).then((role) => {
            return res.json(role);
        }).catch(next);
    });
    
    /**
     * @api {post} /roles Create role
     * @apiGroup Role
     * @apiName Create role
     * @apiSuccess {Object} role Role
     * @apiVersion 2.0.0
     */
    router.post('/', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.role.create(req.body).then((role) => {
            return resolvers.log.create({
                action: 'create',
                data: {},
                entity: role._id,
                entity_name: 'role',
                type: 'info',
                updates: map(role.toObject(), (newValue, fieldName) => {
                    switch (fieldName) {
                        default:
                            return {
                                field_name: fieldName,
                                new_value: newValue,
                                old_value: null,
                            };
                    }
                }),
                user: req.user._id,
            }).then(() => res.json(role));
        }).catch(next);
    });
    
    /**
     * @api {put} /roles/:id Update role by id
     * @apiGroup Role
     * @apiName Update role by Id
     * @apiSuccess {Object} role Role
     * @apiVersion 2.0.0
     */
    router.put('/:id', (req: Request, res: Response, next: NextFunction) => {
        return resolvers.role.get(req.params.id).then((record) => {
            const original = record.toObject();
            return resolvers.role.update(req.params.id, req.body).then((updatedRecord) => {
                const role = updatedRecord.toObject();
                return resolvers.log.create({
                    action: 'update',
                    data: {},
                    entity: role._id,
                    entity_name: 'role',
                    type: 'info',
                    updates: compact(map(original, (oldValue, fieldName) => {
                        switch (fieldName) {
                            case 'permissions':
                                return {
                                    field_name: fieldName,
                                    new_value: filter(role[fieldName], (value: IRolePermission) => {
                                        const found = find(oldValue, ['name', value.name]);
                                        if (found) {
                                            let changed = false;
                                            forEach(value, (keyValue, key) => {
                                                if (!changed) {
                                                    changed = !isEqual(found[key], keyValue);
                                                }
                                            });
                                            return changed;
                                        }
                                        return true;
                                    }),
                                    old_value: filter(oldValue, (value: IRolePermission) => {
                                        const found = find(role[fieldName], ['name', value.name]);
                                        if (found) {
                                            let changed = false;
                                            forEach(value, (keyValue, key) => {
                                                if (!changed) {
                                                    changed = !isEqual(keyValue, found[key]);
                                                }
                                            });
                                            return changed;
                                        }
                                        return true;
                                    }),
                                };
                            default:
                                if (!isEqual(oldValue, role[fieldName])) {
                                    return {
                                        field_name: fieldName,
                                        new_value: role[fieldName],
                                        old_value: oldValue,
                                    };
                                }
                        }
                    })),
                    user: req.user._id,
                }).then(() => res.json(role));
            });
        }).catch(next);
    });
    
    return router;
};
