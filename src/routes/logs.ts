import { NextFunction, Request, Response, Router } from 'express';
import * as moment from 'moment-timezone';
import { IAppConfig } from '../app';
import { IPaginationOptions } from '../lib/Types';
import { IResolvers } from '../resolvers';
import { ILogListParams } from '../resolvers/log';

export default (configs: IAppConfig, resolvers: IResolvers) => {
    const router = Router();
    
    /**
     * @api {get} /logs List Paginated Logs
     * @apiGroup Log
     * @apiName List Paginated Logs
     * @apiSuccess {Object[]} logs Array of logs
     * @apiVersion 2.0.0
     */
    router.get('/', (req: Request, res: Response, next: NextFunction) => {
        const params: ILogListParams = {
            createdAt: {
                $gte: Date.parse(moment().tz(configs.Timezone).startOf('day').toISOString()),
                $lte: Date.parse(moment().tz(configs.Timezone).endOf('day').toISOString()),
            },
        };
        const options: IPaginationOptions = {
            limit: 25,
            page: 1,
            sort: {
                createdAt: 1,
            },
        };
        
        if (req.query.page) {
            options.page = parseInt(req.query.page,  10);
        }
        if (req.query.limit) {
            options.limit = parseInt(req.query.limit,  10);
        }
        if (req.query.sort) {
            options.sort = JSON.parse(req.query.sort);
        }
        if (req.query.start && req.query.end) {
            params.createdAt.$gte = Date.parse(req.query.start);
            params.createdAt.$lte = Date.parse(req.query.end);
        }
        if (req.query.action) {
            params.action = req.query.action;
        }
        if (req.query.entity_name) {
            params.entity_name = req.query.entity_name;
        }
        if (req.query.level) {
            params.level = req.query.level;
        }
        if (req.query.type) {
            params.type = req.query.type;
        }
        return resolvers.log.list(params, options).then((pagination) => {
            pagination.data = pagination.data.map((doc) => {
                return doc.toObject();
            });
            return pagination;
        }).then((pagination) => {
            return Promise.all(pagination.data.map((log) => {
                /**
                 * Process Entity Property
                 */
                if (log.entity && log.entity_name) {
                    switch (log.entity_name) {
                        case 'role':
                            return resolvers.role.get(log.entity).then((entity) => {
                                log.entity = entity.toObject();
                                return log;
                            });
                        case 'user':
                            return resolvers.user.get(log.entity).then((entity) => {
                                log.entity = entity.toObject();
                                return log;
                            });
                        default:
                    }
                }
                return Promise.resolve(log);
            })).then((docs) => {
                pagination.data = docs;
                return pagination;
            });
        }).then((pagination) => res.json(pagination)).catch(next);
    });
    
    return router;
};
