import { Express, NextFunction, Request, Response, static as expressStatic } from 'express';
import { body, validationResult } from 'express-validator/check';
import { pick } from 'lodash';
import { IAppConfig, IErrorResponse } from '../app';
import { IResolvers } from '../resolvers';
import Log from './logs';
import Role from './roles';
import User from './users';

export const Routes = (app: Express, configs: IAppConfig, resolvers: IResolvers): void => {
    /**
     * Implements robots.txt
     */
    app.use('/robots.txt', (req: Request, res: Response) => {
        return res.type('text/plain').send('UserAgent: *\nDisallow: /');
    });
    
    app.use('/docs', expressStatic('docs'));
    
    /**
     * @api {post} /login Authenticate user
     * @apiGroup Authentication
     * @apiName Authenticate user
     * @apiParam {string} username Username of the user
     * @apiParam {string} password Password of the user
     * @apiSuccess {string} token Authentication Token
     * @apiVersion 2.0.0
     */
    app.post('/login', [
        body('username').not().isEmpty().trim().escape().withMessage('Please enter your username'),
        body('password').not().isEmpty().trim().escape().withMessage('Please enter your password'),
    ], (req: Request, res: Response, next: NextFunction) => {
        const errors = validationResult(req).array();
        
        if (errors.length > 0) {
            const response: IErrorResponse = {
                errors,
                message: 'Please enter your username',
                statusCode: 406,
            };
            return next(response);
        }
        return resolvers.user.getByUsername(req.body.username).then((user) => {
            if (!user) {
                return Promise.reject<Response>({
                    message: 'You do not have permission',
                    statusCode: 401,
                });
            }
            return resolvers.user.isAuthorized(user._id).then(() => {
                // TODO: Remove psuedo password check
                if (req.body.password === 'Welcome01') {
                    return res.json({
                        token: resolvers.jwt.createToken(user._id),
                    });
                } else {
                    return Promise.reject<Response>({
                        message: 'Your username/password combination is incorrect',
                        statusCode: 401,
                    });
                }
                // TODO: Add authentication
                // Example: (req.body.username, req.body.password)
                // Response:
                // return res.json({
                //     token: resolvers.jwt.createToken(user._id),
                // });
                // TODO: Add authentication catch
                // .catch(() => {
                //     return Promise.reject<Response>({
                //         message: 'Your username/password combination is incorrect',
                //         statusCode: 401,
                //     });
                // });
            });
        }).catch(next);
    });
    
    /**
     * Validates that a user is logged in
     */
    app.use((req: Request, res: Response, next: NextFunction) => {
        const authHeader = req.header('Authorization') ? req.header('Authorization') : '';

        if (!authHeader) {
            return next({
                message: 'Please make sure your request has an Authorization header',
                statusCode: 401,
            });
        }
        
        const [ schema, token ] = authHeader.split(' ');
        
        if (!schema || !schema.match(/Bearer/i)) {
            return next({
                message: 'Incorrect schema supplied',
                statusCode: 401,
            });
        }
        if (!token) {
            return next({
                message: 'No token supplied',
                statusCode: 401,
            });
        }
        return resolvers.jwt.isAuthorized(token).then((userId) => {
            return resolvers.user.isAuthorized(userId).then((user) => {
                req.user = user;
                return next();
            });
        }).catch(next);
    });
    
    /**
     * @api {get} /me Get logged in user
     * @apiGroup Authentication
     * @apiName Get logged in user
     * @apiSuccess {Object} user User
     * @apiVersion 2.0.0
     */
    app.get('/me', (req: Request, res: Response) => res.json(req.user));
    
    /**
     * All routes
     */
    app.use('/logs', Log(configs, resolvers));
    app.use('/roles', Role(resolvers));
    app.use('/users', User(resolvers));
    
    /**
     * Base Route
     */
    app.use((req: Request, res: Response, next: NextFunction) => {
        return res.type('text/plain').send(`${configs.Name} v${configs.Version}`);
    });
    
    /**
     * Define Error Handling
     */
    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        return resolvers.log.create({
            action: 'request',
            data: pick(req, [
                'body',
                'headers',
                'hostname',
                'protocol',
                'method',
                'query',
                'url',
            ]),
            entity: {
                name: 'Request Error',
            },
            entity_name: 'api',
            error: err,
            type: 'error',
            updates: [],
        }).then(() => {
            return res.status(err.statusCode).send({
                message: err.message,
                statusCode: err.statusCode,
            });
        });
    });
};
