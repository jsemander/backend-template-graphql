import { Document, Schema, Types } from 'mongoose';
import { IUser, IUserModel } from './users';

export interface ISession {
    _id: Types.ObjectId;
    expiration: Date;
    socket: string;
    user: IUser;
}
export interface ISessionModel extends Document {
    createdAt: Date;
    expiration: Date;
    socket: string;
    updatedAt: Date;
    user: IUserModel;
}
export const SessionSchema = new Schema({
    expiration: {
        default: Date.now(),
        required: true,
        type: Date,
    },
    socket: {
        type: String,
    },
    user: {
        ref: 'User',
        required: true,
        type: Schema.Types.ObjectId,
    },
}, {
    timestamps: true,
    versionKey: false,
});
