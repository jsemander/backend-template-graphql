import { Document, Schema } from 'mongoose';

export interface IConfigModel extends Document {
    name: string;
    value: any;
}
export const ConfigSchema = new Schema({
    name: {
        type: String,
    },
    value: {
        type: Schema.Types.Mixed,
    },
}, {
    timestamps: true,
    versionKey: false,
});
