import { Cron } from '../lib/Cron';
import { IResolvers } from '../resolvers';
import { IAppConfig } from './';
import { ITasks } from './tasks';

export interface IBatch {
    cron: Cron;
    resolvers: IResolvers;
}
export const Batch = (configs: IAppConfig, resolvers: IResolvers, tasks: ITasks): IBatch => {
    /**
     * Scheduler object
     */
    const cron = new Cron(configs.Cron, resolvers.job);
    
    /**
     * Define tasks
     */
    Object.keys(tasks).forEach((model) => {
        const actions = tasks[model];
        Object.keys(actions).forEach((action) => {
            cron.define(model, action, actions[action]);
            console.log(`Defined ${model}.${action} successfully`);
        });
    });
    
    /**
     * Return batch objects
     */
    return {
        cron,
        resolvers,
    };
};
