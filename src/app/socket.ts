import { Server } from 'http';
import * as IORedis from 'ioredis';
import * as SocketIO from 'socket.io';
import * as SocketIORedis from 'socket.io-redis';
import { IResolvers } from '../resolvers';
import { IAppConfig } from './';

export interface ISocket {
    io: SocketIO.Server;
}
export const Socket = (configs: IAppConfig, resolvers: IResolvers, server: Server): ISocket => {
    /**
     * Socket Server
     */
    const io = SocketIO(server, {
        serveClient: false,
    });
    
    /**
     * Check if redis is configured and enabled
     */
    if (configs.Redis && configs.Redis.enabled) {
        /**
         * Create socket redis cluster
         */
        switch (configs.Redis.mode) {
            case 'cluster':
                io.adapter(SocketIORedis({
                    pubClient: new IORedis.Cluster(configs.Redis.nodes, configs.Redis.options),
                    subClient: new IORedis.Cluster(configs.Redis.nodes, configs.Redis.options),
                }));
                break;
            case 'single':
            default:
                io.adapter(SocketIORedis(configs.Redis.nodes[0]));
        }
    }
    
    /**
     * Validate Socket token
     */
    io.sockets.on('connection', (socket) => {
        /**
         * JWT token
         */
        const token = socket.handshake.query.token;
        
        /**
         * Validate Client Token
         */
        if (token) {
            return resolvers.jwt.isAuthorized(token).then((sessionId: string) => {
                /**
                 * Valid Token
                 */
                return resolvers.session.get(sessionId).then((session) => {
                    /**
                     * Authorized User
                     */
                    return resolvers.user.isAuthorized(session.user._id).then((user) => {
                        /**
                         * Update socket id if user session exists
                         */
                        return resolvers.session.update(session._id, {
                            socket: socket.id,
                        }).then(() => {
                            /**
                             * Check if user is on another socket
                             */
                            if (session.socket) {
                                /**
                                 * Initiate Force Logout for old user session
                                 */
                                io.to(session.socket).emit('user:sessionExists');
                                socket.emit('session:updated', user);
                            }
                            
                            /**
                             * Return session object
                             */
                            return session;
                        });
                    });
                }).catch((err) => {
                    /**
                     * Unauthorized User
                     */
                    return resolvers.log.create({
                        action: 'request',
                        data: {},
                        entity: {
                            name: 'Unauthorized User',
                        },
                        entity_name: 'system',
                        error: err,
                        type: 'error',
                        updates: [],
                    }).then(() => {
                        /**
                         * Disconnect client from socket
                         */
                        return socket.disconnect();
                    }).catch(() => {
                        /**
                         * Disconnect client from socket
                         */
                        return socket.disconnect();
                    });
                });
            }).catch((err) => {
                /**
                 * Invalid Token
                 */
                return resolvers.log.create({
                    action: 'request',
                    data: {},
                    entity: {
                        name: 'Invalid Token',
                    },
                    entity_name: 'system',
                    error: err,
                    type: 'error',
                    updates: [],
                }).then(() => {
                    /**
                     * Disconnect client from socket
                     */
                    return socket.disconnect();
                }).catch(() => {
                    /**
                     * Disconnect client from socket
                     */
                    return socket.disconnect();
                });
            });
        } else {
            /**
             * Disconnect client from socket
             */
            return socket.disconnect();
        }
    });
    
    /**
     * Return Socket
     */
    return {
        io,
    };
};
