export interface IDateRange {
    $eq?: number;
    $gt?: number;
    $gte?: number;
    $lt?: number;
    $lte?: number;
}
export interface IMongooseParam {
    $addFields?: {
        [key: string]: string;
    };
    $lookup?: {
        as: string;
        foreignField: string;
        from: string;
        localField: string;
    };
    $limit?: number;
    $match?: any;
    $skip?: number;
    $sort?: {
        [key: string]: number;
    };
    $unwind?: {
        path: string;
        preserveNullAndEmptyArrays: boolean;
    };
}
export interface IPagination<T> {
    data: T[];
    meta: {
        pagination: {
            count: number;
            total: number;
        };
    };
}
export interface IPaginationOptions {
    limit: number;
    page: number;
    sort: {
        [key: string]: number;
    };
}
export interface IPromiseError {
    message: string;
    statusCode: number;
}
export interface ITaskResponse {
    error?: string | object | Error;
    success: boolean;
}
