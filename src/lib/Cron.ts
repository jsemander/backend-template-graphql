import { EventEmitter } from 'events';
import { Job } from '../resolvers/job';
import { ITaskResponse } from './Types';

export interface ICronConfig {
    timeout?: number;
}
export class Cron extends EventEmitter {
    protected resolver: Job;
    protected running = false;
    protected tasks: {
        [key: string]: {
            [key: string]: (...args: any[]) => Promise<ITaskResponse>;
        };
    } = {};
    protected timeout: number;
    
    /**
     * Class Constructor
     */
    constructor(options: ICronConfig, resolver: Job) {
        super();
        this.resolver = resolver;
        if (options.timeout) {
            this.timeout = options.timeout;
        } else {
            this.timeout = 1000;
        }
    }
    
    /**
     * Add new task to task list
     */
    public define(model: string, action: string, method: (...args: any[]) => Promise<ITaskResponse>): boolean {
        if (!model || !action || !method) {
            return false;
        }
        if (this.tasks[model] !== undefined) {
            this.tasks[model][action] = method;
        } else {
            this.tasks[model] = {};
            this.tasks[model][action] = method;
        }
        return true;
    }
    
    /**
     * Starts the scheduler
     */
    public start(): Promise<string> {
        return new Promise((resolve, reject) => {
            if (!this.running) {
                this.running = true;
                this.nextTick();
                return resolve('The scheduler has started');
            } else {
                return reject('The scheduler is already running');
            }
        });
    }
    
    /**
     * Stops the scheduler
     */
    public stop(): Promise<string> {
        return new Promise((resolve, reject) => {
            if (this.running) {
                this.running = false;
                return resolve('The scheduler has shut down');
            } else {
                return reject('The scheduler is not currently running');
            }
        });
    }
    
    /**
     * Processes the next task ready to run
     */
    protected nextTask(): Promise<void> {
        const self = this;
        
        // Get next job to run
        return self.resolver.getNext().then((job) => {
            if (!job) {
                return self.nextTick();
            }
            
            const data = job.data;
            const task = job.task;
            
            // Check if job model exists
            if (this.tasks.hasOwnProperty(task.model) && this.tasks[task.model].hasOwnProperty(task.action)) {
                const model = task.model;
                const action = task.action;
                
                // Start job
                self.emit('start', job);
                return this.resolver.running(job._id).then(() => {
                    return this.tasks[model][action](data).then((result: ITaskResponse) => {
                        // Process job status
                        if (result.success) {
                            self.emit('success', job);
                        } else {
                            self.emit('fail', result.error, job);
                        }
                        // Check for next job to run
                        return this.resolver.finish(job._id).then(() => self.nextTick());
                    });
                }).catch((err) => {
                    self.emit('fail', err, job);
                    return this.resolver.finish(job._id).then(() => self.nextTick());
                });
            }
            
            // Mark job as disabled if task is not defined
            return this.resolver.disable(job._id).then((record) => {
                self.emit('error', {
                    message: `${record.name} has been disabled`,
                });
                // Check for next job to run
                return self.nextTick();
            });
        }).catch((err) => {
            self.emit('error', err);
            return self.nextTick();
        });
    }
    
    /**
     * Sets up the next check for a task
     */
    protected nextTick() {
        if (this.running) {
            const self = this;
            process.nextTick(() => self.nextTask());
        }
    }
}
