import { paginateRecords } from '../lib/Pagination';
import { Resolver } from '../lib/Resolver';
import { IPagination, IPaginationOptions } from '../lib/Types';
import { IModels } from '../models';
import { IRoleModel, IRolePermission } from '../models/roles';

export interface IRoleCreateParams {
    name: string;
    permissions: IRolePermission[];
}
export interface IRoleListParams {
    enabled?: boolean;
}
export interface IRoleUpdateParams {
    enabled?: boolean;
    name?: string;
    permissions?: IRolePermission[];
}
export class Role extends Resolver<IModels> {
    /**
     * Create role
     */
    public create(data: IRoleCreateParams): Promise<IRoleModel> {
        return this.models.role.findOne({
            name: new RegExp(`^${data.name}$`, 'i'),
        }).then((result) => {
            if (result) {
                return Promise.reject<IRoleModel>({
                    message: `A role already exists with the same name: ${result.name}`,
                    statusCode: 409,
                });
            } else {
                const role = new this.models.role(data);
                return role.save();
            }
        });
    }
    
    /**
     * Get role by id
     */
    public get(id: string): Promise<IRoleModel> {
        return this.models.role.findById(id).then((role) => {
            if (!role) {
                return Promise.reject<IRoleModel>({
                    message: 'No role found',
                    statusCode: 404,
                });
            }
            return role;
        });
    }
    
    /**
     * Get role by name
     */
    public getByName(name: string): Promise<IRoleModel> {
        return this.models.role.findOne({ name }).then((role) => {
            if (!role) {
                return Promise.reject<IRoleModel>({
                    message: 'No role found',
                    statusCode: 404,
                });
            }
            return role;
        });
    }
    
    /**
     * Retrieves a list of roles by params
     */
    public list(params: IRoleListParams, options: IPaginationOptions): Promise<IPagination<IRoleModel>> {
        return paginateRecords(this.models.role, params, options.sort, [], [], options.page, options.limit);
    }
    
    /**
     * Update role
     */
    public update(id: string, data: IRoleUpdateParams): Promise<IRoleModel> {
        return this.models.role.findOne({
            _id: { $ne: id },
            name: new RegExp(`^${data.name}$`, 'i'),
        }).then((result) => {
            if (result) {
                return Promise.reject<IRoleModel>({
                    message: `A role already exists with the same name: ${result.name}`,
                    statusCode: 409,
                });
            } else {
                return this.models.role.findById(id).then((role) => {
                    if (!role) {
                        return Promise.reject<IRoleModel>({
                            message: 'No role found',
                            statusCode: 404,
                        });
                    }
                    role.set(data);
                    return role.save().then(() => this.get(id));
                });
            }
        });
    }
}
