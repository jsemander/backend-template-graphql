import { paginateRecords } from '../lib/Pagination';
import { Resolver } from '../lib/Resolver';
import { IDateRange, IPagination, IPaginationOptions } from '../lib/Types';
import { IModels } from '../models';
import { ILogModel, ILogUpdate } from '../models/log';

export interface ILogCreateParams {
    action: string;
    data?: any;
    entity?: any;
    entity_name: string;
    error?: any;
    level?: string;
    type: string;
    updates: ILogUpdate[];
    user?: any;
}
export interface ILogListParams {
    action?: string;
    createdAt: IDateRange;
    entity_name?: string | { [key: string]: any; };
    level?: string;
    type?: string;
    user?: {
        $eq?: string;
        $in?: string[];
    };
}
export class Log extends Resolver<IModels> {
    /**
     * Create log
     */
    public create(data: ILogCreateParams): Promise<ILogModel> {
        const log = new this.models.log(data);
        switch (data.entity_name) {
            case 'api':
            case 'cron':
            case 'email':
            case 'job':
            case 'system':
                log.level = 'system';
                break;
            default:
                log.level = 'data';
        }
        return log.save().then((record: ILogModel) => this.get(record._id));
    }
    
    /**
     * Get log by id
     */
    public get(id: string): Promise<ILogModel> {
        return this.models.log.findById(id).then((log) => {
            if (!log) {
                return Promise.reject<ILogModel>({
                    message: 'No log found',
                    statusCode: 404,
                });
            }
            return log;
        });
    }
    
    /**
     * Retrieves a list of logs by params
     */
    public list(params: ILogListParams, options: IPaginationOptions): Promise<IPagination<ILogModel>> {
        return paginateRecords(this.models.log, params, options.sort, [{
            as: 'user',
            foreignField: '_id',
            from: 'users',
            localField: 'user',
        }], [{
            path: '$user',
            preserveNullAndEmptyArrays: true,
        }], options.page, options.limit);
    }
}
