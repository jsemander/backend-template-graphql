import * as moment from 'moment';
import { Resolver } from '../lib/Resolver';
import { IModels } from '../models';
import { ISessionModel } from '../models/sessions';

export interface ISessionRemoveParams { }
export interface ISessionUpdateParams { }
export class Session extends Resolver<IModels> {
    /**
     * Create session
     */
    public create(userId: string, socketId?: string): Promise<ISessionModel> {
        return this.removeByUser(userId).then(() => {
            return this.models.user.findById(userId).then((user) => {
                if (!user) {
                    return Promise.reject<any>({
                        message: 'There was no user record found',
                        statusCode: 404,
                    });
                }
                if (!user.enabled) {
                    return Promise.reject<any>({
                        message: 'Your user has been disabled',
                        statusCode: 404,
                    });
                }
                if (!user.role) {
                    return Promise.reject<any>({
                        message: 'Your permission has been revoked',
                        statusCode: 404,
                    });
                }
                const session = new this.models.session({
                    expiration: new Date(moment().add({ day: 1 }).toISOString()),
                    socket: socketId,
                    user: userId,
                });
                return session.save();
            });
        });
    }
    
    /**
     * Get session by id
     */
    public get(id: string): Promise<ISessionModel> {
        return this.models.session.findById(id).populate({
            path: 'user',
            populate: {
                path: 'role',
            },
        }).then((session) => {
            if (!session) {
                return Promise.reject<any>({
                    message: 'There was no session record found',
                    statusCode: 404,
                });
            }
            if (!session.user) {
                return this.removeById(id).then(() => {
                    return Promise.reject<any>({
                        message: 'There was no user record found',
                        statusCode: 404,
                    });
                });
            }
            if (!session.user.enabled) {
                return this.removeById(id).then(() => {
                    return Promise.reject<any>({
                        message: 'Your user has been disabled',
                        statusCode: 404,
                    });
                });
            }
            if (!session.user.role) {
                return this.removeById(id).then(() => {
                    return Promise.reject<any>({
                        message: 'Your permission has been revoked',
                        statusCode: 404,
                    });
                });
            }
            if (!moment(session.expiration.toISOString()).isAfter(moment())) {
                return this.removeById(id).then(() => {
                    return Promise.reject<any>({
                        message: 'The session is expired',
                        statusCode: 409,
                    });
                });
            }
            return session;
        });
    }
    
    /**
     * Get session by socket id
     */
    public getBySocketId(socketId: string): Promise<ISessionModel | null> {
        return this.models.session.findOne({ socket: socketId }).then((socket) => socket);
    }
    
    /**
     * Get session by user id
     */
    public getByUserId(userId: string): Promise<ISessionModel | null> {
        return this.models.session.findOne({ user: userId }).then((socket) => socket);
    }
    
    /**
     * Delete session by params
     */
    public remove(params: ISessionRemoveParams): Promise<boolean> {
        return this.models.session.deleteMany(params).then(() => true);
    }
    
    /**
     * Delete session by id
     */
    public removeById(id: string): Promise<boolean> {
        return this.models.session.deleteMany({ _id: id }).then(() => true);
    }
    
    /**
     * Delete session by user id
     */
    public removeByUser(userId: string): Promise<boolean> {
        return this.models.session.deleteMany({ user: userId }).then(() => true);
    }
    
    /**
     * Update session
     */
    public update(id: string, data: ISessionUpdateParams): Promise<ISessionModel> {
        return this.models.session.findById(id).then((session) => {
            if (!session) {
                return Promise.reject<ISessionModel>({
                    message: 'No session found',
                    statusCode: 404,
                });
            }
            session.set(data);
            return session.save();
        });
    }
}
