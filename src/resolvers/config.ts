import { Resolver } from '../lib/Resolver';
import { IModels } from '../models';
import { IConfigModel } from '../models/config';

export class Config extends Resolver<IModels> {
    /**
     * Get config by name
     */
    public getByName(name: string): Promise<IConfigModel> {
        return this.models.config.findOne({ name }).then((config) => {
            if (!config) {
                return Promise.reject<IConfigModel>({
                    message: 'No config found',
                    statusCode: 404,
                });
            }
            return config;
        });
    }
    
    /**
     * Retrieves a list of configs by params
     */
    public list(params: any): Promise<IConfigModel[]> {
        return this.models.config.find(params).then((configs) => configs);
    }
}
